const express= require('express');

const app=express();

app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.set('port',3000);
// comentario
app.get('/',(req,res)=>{
    res.send("WELCOME TO OUR API");
});
app.use("/errores",require("./routes/errores"));
app.use("/errores/data",require("./routes/errores"));

app.use("errores/user",require("./routes/errores"));

app.use("errores/user/:id_user",require("./routes/errores"));

app.use("errores/data/:error_machine",require("./routes/errores"));

app.listen(app.get('port'));
console.log(`Server running on port ${app.get('port')}`)
