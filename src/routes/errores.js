const {Router} = require("express");
const router = Router();
const{getError,getData,getDateByErrorData,deleteUser,getUser,addUser,updateUser}=require("../controller/controller");

router.get('/data', getData);
router.get('/',getError)
router.get('/user',getUser);
router.post('/user',addUser);
router.put("/user/:id_user",updateUser);
router.delete("/user/:id_user",deleteUser);
router.get("/data/:error_machine",getDateByErrorData);

module.exports=router;