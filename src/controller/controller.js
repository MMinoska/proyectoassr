const dbConnection= require('../config/databaseCon');
const connection=dbConnection();

let getError=async(req,res)=>{
    await connection.query("select * from error_machine",(err,result)=>{
        if (result)
            res.send(result);
        else
            res.status(500).send(err);
    });

}

let getUser=async(req,res)=>{
    await connection.query("SELECT * FROM user", (err,result)=>{
        if (result)
            res.send(result)
        else
            res.status(500).send(err);
    })
}

let getData=async(req,res)=>{
    await connection.query("SELECT * FROM data", (err,result)=>{
        if (result)
            res.send(result)
        else
            res.status(500).send(err);
    })
}

let getDateByErrorData =async(req,res)=>{
    const {error_machine}= req.params;
    const sql= `SELECT date_error FROM data
    WHERE error_machine = ${error_machine}`;
    await connection.query(sql,(err,result)=>{
        if (result)
            res.send(result)
        else
            res.status(500).send(err);
    })
}

let addUser= async(req,res)=>{
    const{id_user,nombre,apellido,telefono,email}=req.body
    await connection.query(`INSERT INTO user VALUES(${id_user},'${nombre}','${apellido}','${telefono}','${email}')`,(err,result)=>{
        if (result)
            res.send('USER WAS SUCCESFULLY CREATED !');
        else
            res.status(500).send(err);
    }) 
}


let updateUser= async(req,res)=>{
    const {id_user}= req.params;
    const {nombre,apellido,telefono,email}= req.body;
    const sql= `UPDATE user SET nombre = '${nombre}', apellido = '${apellido}',
    telefono = '${telefono}', email = '${email}' 
    WHERE id_user = ${id_user}`;

    connection.query(sql, error =>{
        if (error) throw error;
        res.send('USER WAS SUCCESFULLY UPDATED !')
    })
}

let deleteUser= async(req,res)=>{
    const {id_user}= req.params;
    const sql= `DELETE FROM user WHERE id_user = ${id_user}`;

    connection.query(sql, error =>{
        if (error) throw error;
        res.send('USER WAS SUCCESFULLY DELETED')
    })
}


module.exports={
    getError,
    addUser,
    getData,
    getUser,
    updateUser,
    deleteUser,
    getDateByErrorData
}